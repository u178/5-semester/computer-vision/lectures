import numpy as np
import matplotlib.pyplot as plt
from skimage.measure import label, regionprops
import tqdm


def lakes_and_bays(image):
    b = ~image
    lb = label(b)
    regs = regionprops(lb)
    count_lakes = 0
    count_bays = 0
    for reg in regs:
        flag = True
        for y, x in reg.coords:
            if y == 0 or x == 0 or y == image.shape[0] - 1 or x == image.shape[1] - 1:
                flag = False
                break
        if flag:
            count_lakes += 1
        else:
            count_bays += 1

    return count_lakes, count_bays


def has_vline(region):
    lines = np.sum(region.image, 0) // region.image.shape[0]
    return 1 in lines


def filling_factor(region):
    return np.sum(region.image) / region.image.size


def recognize(region):
    if np.all(region.image):
        return '-'

    lakes, bays = lakes_and_bays(region.image)
    if lakes == 2:
        if has_vline(region):
            return 'B'
        else:
            return '8'
    elif lakes == 1:
        if bays == 3:
            return 'A'
        else:
            return '0'
    elif lakes == 0:
        if has_vline(region):
            return '1'
        elif bays == 2:
            return '/'
        cut_cl, cut_cb = lakes_and_bays(region.image[2:-2, 2:-2])
        if cut_cb == 4:
            return 'x'
        if cut_cb == 5:
            cy = region.image.shape[0] // 2
            cx = region.image.shape[1] // 2
            if region.image[cy, cx] > 0:
                return '*'
            else:
                return 'w'
    return None


image = plt.imread("../data/images/alphabet.png")
binary = np.sum(image, 2)
binary[binary > 0] = 1

labeled = label(binary)
regions = regionprops(labeled)

d = {None: 0}

for region in regions:
    symbol = recognize(region)
    if symbol is not None:
        labeled[np.where(labeled == region.label)] = 0
    else:
        print(filling_factor(region))
    if symbol not in d:
        d[symbol] = 1
    else:
        d[symbol] += 1

print(round((1. - d[None] / sum(d.values())) * 100, 2))
print(d)

# print(lakes(regions[60]))
# print(has_vline(regions[60]))
# print(regions[60].image)
# print(regions[0].area)

# 164 - star, 153 - w
plt.imshow(labeled, cmap="gray")
# plt.imshow(regions[164].image, cmap="gray")
# plt.show()
# print(np.max(labeled))
