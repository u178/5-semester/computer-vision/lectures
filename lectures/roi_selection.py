import cv2.cv2 as cv2  # Could be import cv2 for you
import numpy as np

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
# ROI - Region Of Interest
cv2.namedWindow("ROI", cv2.WINDOW_KEEPRATIO)

roi = None


while cam.isOpened():
    ret, image = cam.read()

    if roi is not None:
        res = cv2.matchTemplate(gray, roi, cv2.TM_CCORR_NORMED)
        # cv2.imshow("MATCH template", res)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        top_left = max_loc
        bottom_right = (top_left[0] + roi.shape[1],
                        top_left[1] + roi.shape[0])
        cv2.rectangle(image, top_left, bottom_right, 255, 2)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # transform to gray
    gray = cv2.GaussianBlur(gray, (21, 21), 0)  # blur img

    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break

    if key == ord('r'):
        """
            press r
            select ROI object by drawing a rect
            then press enter to close the ROI selection window
        """
        r = cv2.selectROI("ROI selection", gray)
        roi = gray[int(r[1]): int(r[1] + r[3]),
                   int(r[0]): int(r[0] + r[2])]
        cv2.imshow("ROI", roi)
        cv2.destroyWindow("ROI selection")
    cv2.imshow("Camera", gray)





cam.release()
cv2.destroyAllWindows()
