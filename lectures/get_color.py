import cv2.cv2 as cv2
import numpy as np

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera is not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)


position = []


def on_mouse_click(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDOWN:
        global position
        print(position)
        position = [y, x]


cv2.setMouseCallback("Camera", on_mouse_click)

measures = []
bgr_color = []
hsv_color = []

while cam.isOpened():
    _, image = cam.read()
    if position:
        px1 = image[position[0], position[1]]
        measures.append(px1)
        if len(measures) >= 10:
            bgr_color = np.uint8([[np.average(measures, 0)]])
            hsv_color = cv2.cvtColor(bgr_color, cv2.COLOR_BGR2HSV)
            bgr_color = bgr_color[0, 0]
            hsv_color = hsv_color[0, 0]
            measures.clear()
        cv2.circle(image, (position[1], position[0]), 5, (0, 0, 255), 2)
        cv2.putText(image, f"BGR color: {bgr_color}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 255))
        cv2.putText(image, f"HSV color: {hsv_color}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 255))
    cv2.imshow("Camera", image)

    """
    for my cam colors are:
    yellow - [23, 198, 198]
    green - [60, 130, 160]
    red - [178, 225, 150]
    """
    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break



cam.release()
cv2.destroyAllWindows()
