import math
import random

import cv2.cv2 as cv2
import numpy as np
import time

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera is not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)

"""
lower =  [hue[0] - delta, hue[1] - delta1, hue[2] - delta1] 
upper =  [hue[0] + delta, 255, 255] 
where:
    delta ≈≈ 7
    delta1 ≈≈ 25
"""

# green color
lower_g = (51, 90, 120)
upper_g = (70, 255, 255)

lower_y = (20, 160, 170)
upper_y = (30, 255, 255)

lower_r = (0, 150, 140)
upper_r = (15, 255, 255)

VICTORY_FLAG = False


def guess_colors(colors):
    c = colors.copy()
    random.shuffle(c)
    return c


def get_order(green, yellow, red):
    color_list = [(green[0][0], 'g'), (yellow[0][0], 'y'), (red[0][0], 'r')]
    color_list.sort()
    return color_list[0][1], color_list[1][1], color_list[2][1]


def get_circle(hsv, lower, upper):
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)
        (curr_x, curr_y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            return (curr_x, curr_y), radius
    return None


colors_to_guess = ['g', 'y', 'r']
guessed_colors = tuple(guess_colors(colors_to_guess))
print(guessed_colors)

while cam.isOpened():
    _, image = cam.read()
    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    green_circle = get_circle(hsv, lower_g, upper_g)
    yellow_circle = get_circle(hsv, lower_y, upper_y)
    red_circle = get_circle(hsv, lower_r, upper_r)

    if green_circle is not None:
        cv2.circle(blurred, (int(green_circle[0][0]), int(green_circle[0][1])), int(green_circle[1]), (0, 255, 0), 3)
        cv2.circle(blurred, (int(green_circle[0][0]), int(green_circle[0][1])), 3, (0, 255, 0), 3)

    if yellow_circle is not None:
        cv2.circle(blurred, (int(yellow_circle[0][0]), int(yellow_circle[0][1])), int(yellow_circle[1]), (0, 255, 255), 3)
        cv2.circle(blurred, (int(yellow_circle[0][0]), int(yellow_circle[0][1])), 3, (0, 255, 255), 3)

    if red_circle is not None:
        cv2.circle(blurred, (int(red_circle[0][0]), int(red_circle[0][1])), int(red_circle[1]), (0, 0, 255), 3)
        cv2.circle(blurred, (int(red_circle[0][0]), int(red_circle[0][1])), 3, (0, 0, 255), 3)

    if green_circle and yellow_circle and red_circle:
        ordered_colors = get_order(green_circle, yellow_circle, red_circle)
        # print(ordered_colors)
        if ordered_colors == guessed_colors:
            VICTORY_FLAG = True
    if VICTORY_FLAG:
        cv2.putText(blurred, "Congratulations, You've won!", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255))

    cv2.imshow("Camera", blurred)

    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break

cam.release()
cv2.destroyAllWindows()
