import math

import cv2.cv2 as cv2
import numpy as np
import time

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera is not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)

"""
lower =  [hue[0] - delta, hue[1] - delta1, hue[2] - delta1] 
upper =  [hue[0] + delta, 255, 255] 
where:
    delta ≈≈ 7
    delta1 ≈≈ 25
"""
# green color
lower = (51, 90, 120)
upper = (70, 255, 255)

prev_time = time.time()
cur_time = time.time()
prev_x, prev_y = 0, 0
curr_x, curr_y = 0, 0
d = 5.6 * 10 ** -2  # diameter of the ball
radius = 1

while cam.isOpened():
    _, image = cam.read()
    cur_time = time.time()
    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    if len(cnts) > 0:
        c = max(cnts, key=cv2.contourArea)
        (curr_x, curr_y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(image, (int(curr_x), int(curr_y)), int(radius), (0, 255, 0), 3)
            cv2.circle(image, (int(curr_x), int(curr_y)), 3, (0, 255, 0), 3)

    time_diff = cur_time - prev_time
    # if radius is not None and radius > 0:
    pxl_per_m = d / radius
    dist = math.sqrt((curr_x - prev_x) ** 2 + (curr_y - prev_y) ** 2)
    speed = dist / time_diff * pxl_per_m

    cv2.putText(image, f"Instant speed: {speed:.4f}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 255))

    cv2.imshow("Camera", image)
    cv2.imshow("Mask", mask)

    prev_time = cur_time
    prev_x = curr_x
    prev_y = curr_y

    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break

cam.release()
cv2.destroyAllWindows()
