import cv2.cv2 as cv2  # Could be import cv2 for you
import numpy as np

FRAMES_THRESHOLD = 10
STD_THRESHOLD = 2.5
BUFFER_SIZE = 10
thd = 450

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Background", cv2.WINDOW_KEEPRATIO)
background = None
prev_gray = None
buffer = []
frames = 0


while cam.isOpened():
    ret, image = cam.read()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # transform to gray
    gray = cv2.GaussianBlur(gray, (21, 21), 0)  # blur img
    if frames % FRAMES_THRESHOLD == 0:
        if prev_gray is not None:
            diff = prev_gray - gray
            buffer.append(diff.mean())
            if len(buffer) > BUFFER_SIZE:
                buffer.pop(0)
                std = np.std(buffer)
                print(std)
                if std < STD_THRESHOLD:
                    print(f"updated back, std: {std}")
                    background = gray.copy()
                    # buffer.clear()

    key = cv2.waitKey(1)
    if key == ord('q'):
        break

    # if key == ord('b'):
    #     background = gray.copy()

    if background is not None:
        delta = cv2.absdiff(background, gray)
        thresh = cv2.threshold(delta, 25, 255, cv2.THRESH_BINARY)[1]  # otsu is too slow
        thresh = cv2.dilate(thresh, None, iterations=2)
        contours, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area > thd:
                (x, y, w, h) = cv2.boundingRect(cnt)
                cv2.rectangle(gray, (x, y), (x+w, y+h), (0, 255, 0), 2)

        cv2.imshow('Background', thresh)
    cv2.imshow("Camera", gray)
    prev_gray = gray
    frames += 1

cam.release()
cv2.destroyAllWindows()
