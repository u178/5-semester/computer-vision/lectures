import math
import random

import cv2.cv2 as cv2
import numpy as np
import time

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera is not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Cnts", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Gray", cv2.WINDOW_KEEPRATIO)


background = None

_, image = cam.read()
print()
hh, ww = image.shape[:2]

lower = np.array([200, 200, 200])
upper = np.array([255, 255, 255])



while cam.isOpened():
    test_img = np.zeros((hh, ww), dtype=np.uint8)
    _, image = cam.read()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)

    if background is not None:
        bin_back = cv2.absdiff(gray, background)

        _, thresh = cv2.threshold(bin_back, 10, 255, 0)
        bin_img = cv2.Canny(bin_back, 100, 200)

        thresh = bin_img.copy()
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (20, 20))
        morph = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        contours = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]

        # draw white contours on black background as mask
        mask = np.zeros((hh, ww), dtype=np.uint8)
        for cntr in contours:
            cv2.drawContours(test_img, [cntr], 0, (255, 255, 255), -1)
            # print(cntr)




    # _, thresh = cv2.threshold(bin_img, 10, 255, 0)
    # cnts, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # paper = cnts[0]
    #
    # image2 = image.copy()
    # cv2.drawContours(image2, cnts[0], -1, (255, 0, 0), 3)
    #
    # cv2.drawContours(image, cnts, -1, (255, 0, 0), 3)




    # print(cnts)
    cv2.imshow("Camera", image)
    if background is not None:
        cv2.imshow("Cnts", bin_back)
        cv2.imshow("Gray", test_img)

    # rect = cv2.minAreaRect(cnts)
    # box = cv2.boxPoints(rect)
    # box = np.int0(box)
    # cv2.drawContours(image, [box], 0, (0, 255, 0), 2)



    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break
    elif key == ord('b'):
        background = gray.copy()

    # time.sleep(1000)
cam.release()
cv2.destroyAllWindows()
