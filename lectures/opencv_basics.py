import cv2
import cv2.cv2
import numpy as np

def show_img(img, type=cv2.WINDOW_KEEPRATIO):
    cv2.namedWindow("Image", type)
    cv2.imshow("Image", img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


def show_img_task():
    image = cv2.imread("../data/images/cvlogo.png")
    show_img(image, cv2.WINDOW_FULLSCREEN)


def show2img():
    mush = cv2.imread("../data/images/mushroom.jpg")  # loads BGR image
    logo = cv2.imread("../data/images/cvlogo.png")  # loads BGR image
    logo = cv2.resize(logo, (logo.shape[0]//2, logo.shape[1]//2))  # shape[0] = rows, shape[1] = columns

    gray = cv2.cvtColor(logo, cv2.COLOR_BGR2GRAY)  # to gray
    retval, mask = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)  # removing background

    roi = mush[:logo.shape[0], :logo.shape[1]]  # region of interests
    mask_inv = cv2.bitwise_not(mask)

    bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    fg = cv2.bitwise_and(logo, logo, mask=mask)
    combined = cv2.add(fg, bg)
    mush[:combined.shape[0], :combined.shape[1]] = combined

    show_img(mush)


def detect_all_red_objects():
    rose = cv2.imread("../data/images/rose.jpg")  # loads BGR image

    hsv = cv2.cvtColor(rose, cv2.COLOR_BGR2HSV)
    lower = np.array([0, 200, 100])
    upper = np.array([0, 255, 255])

    mask = cv2.inRange(hsv, lower, upper)
    res = cv2.bitwise_and(rose, rose, mask=mask)

    show_img(res)


def affine_transformations():
    news = cv2.imread("../data/images/news.jpg")  # loads BGR image
    chebu = cv2.imread("../data/images/cheburashka.jpg")  # loads BGR image

    rows, cols, _ = chebu.shape
    pts1 = np.float32([[0, 0], [0, rows], [cols, 0], [cols, rows]])  # list of chebu points pairs
    pts2 = np.float32([[19, 25], [39, 294], [434, 51], [434, 261]]) # list of tv points pairs

    M = cv2.getPerspectiveTransform(pts1, pts2)  # get affine matrix

    # print(M)
    aff_img = cv2.warpPerspective(chebu, M, (cols, rows))[:300, :470]  # transform chebu with matrix

    news[:aff_img.shape[0], :aff_img.shape[1]] = aff_img

    show_img(news)
    # ToDo finish transform


affine_transformations()
