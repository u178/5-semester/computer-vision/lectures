import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage import filters
from skimage.measure import regionprops, label

import glob

path = '../data/pencils/img (*).jpg'


def binarize(img):
    img = np.mean(img, 2)
    threshold = filters.threshold_otsu(img)
    img[img < threshold] = 0
    img[img > 0] = 1
    img  = skimage.util.invert(img)
    return img


def get_pencils(b_img):
    labeled = label(b_img)
    regions = regionprops(labeled)
    pencils = 0
    for r in regions:
        if (r.image.shape[0] ** 2 + r.image.shape[1] ** 2) ** 0.5 >= 2000 \
                and r.image.shape[0] > 100 and r.image.shape[1] > 100:
            pencils += 1
    return pencils


def count_pencils(img):
    bimg = binarize(img)
    return get_pencils(bimg)


pencils = 0

for image_path in glob.glob(path):
    img = plt.imread(image_path)
    pencils_on_img = count_pencils(img)
    pencils += pencils_on_img
    print(f'img: {image_path}, pencils: {pencils_on_img}')

print(f'\n---\ntotal: {pencils}')
