import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import skimage
from skimage import color, filters
from skimage.measure import label, regionprops
# matplotlib.use('Qt5Agg')
matplotlib.matplotlib_fname()


# dynamic epsilon
def hue2colors2(img):
    c = np.unique(img[:, :, 0])
    colors = []
    e = np.diff(c).mean()
    prev_color = -1
    a = []

    for i in c:
        if prev_color == -1:
            a.append(i)
        elif np.abs(i - prev_color) >= e:
            mean = np.mean(a)
            if mean != 0.0:
                colors.append(mean)
            a = [i]
        else:
            a.append(i)
        prev_color = i

    colors.append(np.mean(a))
    return colors


def binarize(img):
    img = np.mean(img, 2)
    threshold = filters.threshold_otsu(img)
    img[img < threshold] = 0
    img[img > 0] = 1
    img = skimage.util.invert(img)
    return img


def get_color_index(color: np.array, colors: np.array):
    max_abs = 1
    original_color = 0
    c = color[-1]
    for i in range(len(colors)):
        if abs(colors[i] - c) < max_abs:
            max_abs = abs(colors[i]-c)
            original_color = abs(colors[i])
    return original_color


path = '../data/images/balls_and_rects.png'

image = plt.imread(path)
image = color.rgb2hsv(image)

bin_image = binarize(image)
bin_image = skimage.util.invert(bin_image)
labeled = label(bin_image)

regions = regionprops(labeled)

colors = hue2colors2(image)


figures = {
    'rectangle': {
        'count': 0
    },
    'circle': {
        'count': 0
    }
}

for region in regions:
    sh = region.bbox
    region_image = image[sh[0]:sh[2], sh[1]:sh[3]]
    region_colors = np.unique(region_image[:, :, 0])
    c = str(get_color_index(region_colors, colors))
    if region.extent == 1:
        figures['rectangle']['count'] += 1
        if c in figures['rectangle']:
            figures['rectangle'][c] += 1
        else:
            figures['rectangle'][c] = 1
    else:
        figures['circle']['count'] += 1
        if c in figures['circle']:
            figures['circle'][c] += 1
        else:
            figures['circle'][c] = 1

print(f"colors: {colors}")

print(f"rectangle: {figures['rectangle']}")

print(f"circle: {figures['circle']}")
