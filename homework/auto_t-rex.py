import numpy as np
import cv2.cv2 as cv2
from mss import mss
import pyautogui

right_object_x = 150
left_object_x = 88
DEBUG = True
counter = 1

# game screen coords
monitor = {"top": 240, "left": 650, "width": 600, "height": 150}

# setting pyautogui delay to zero
pyautogui.PAUSE = 0


down_flag = False
up_flag = False

with mss() as sct:
    while True:
        img = cv2.cvtColor(np.array(sct.grab(monitor)), cv2.COLOR_BGRA2GRAY)
        if 83 in img[115, left_object_x:right_object_x]:
            print("jump")
            if down_flag:
                pyautogui.keyUp('down')
                down_flag = False

            pyautogui.keyDown('up')
            up_flag = True

        else:
            cv2.putText(img, f"down", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 255))
            if up_flag:
                pyautogui.keyUp('up')
                up_flag = False

            pyautogui.keyDown('down')
            down_flag = True
        counter += 1
        if DEBUG:
            cv2.imshow("OpenCV/Numpy normal", img)

        # Press "q" to quit
        if cv2.waitKey(25) & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            break

        if counter % 200 == 0:
            right_object_x += 4
            if counter % 400 == 0:
                right_object_x += 2
                left_object_x += 3
